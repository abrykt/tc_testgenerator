from optparse import OptionParser
from tc_testgenerator.tc_testgen import TcTestGen


def main():

    usage = """
    usage: %prog -n <problem name> -u <username>
    usage: %prog -n <problem name> -u <username> -p <password>
    usage: %prog -p <path to file containing definition of testable and test data>
    """
    description = """
    Search the specified problem of TopCoder, and creates testcases for C++
    solutions. If you specify parameter -p <path to file containing definition of 
    testable and testdata> then that is used to create the test (no username and password is required)"""

    parser = OptionParser(usage=usage, description=description)
    parser.add_option('-f', '--file', dest='file', help='Path to file containing definition of testable and test data.')
    parser.add_option('-n', '--problem_name', dest='problem_name', help='Name of the problem to search Topcoder for.')
    parser.add_option('-u', '--username', dest='username', help='user name of topcoder account')
    parser.add_option('-p', '--password', dest='password', help='password for topcoder account')
    parser.add_option('-s', '--path_for_saving', dest='path_for_saving', help='Path where data should be saved.')

    problem_name, username, password, path_to_file, path_for_saving = TcTestGen.get_input_parameters(parser=parser)

    if problem_name:
        TcTestGen.create_from_name(problem_name=problem_name,
                                   user_name=username,
                                   password=password,
                                   path_for_saving=path_for_saving)
    else:
        TcTestGen.create_from_file(path_to_file=path_to_file)


if __name__ == '__main__':
    main()
