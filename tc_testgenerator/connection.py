import requests
import json


class Response(object):
    def __init__(self, response):
        self.response = response

    def read(self):
        """
        Call this to retrieve the response of the query.
        :return:    Returns the response as a string.
        """
        return self.response.text


class Opener(object):
    def __init__(self, session):
        self.session = session

    def open(self, url):
        """
        Call this method with a url to get back a response that you can use to read the content of the page.
        :param url:     The url of the page.
        :return:        An instance of the Response class. Call read(9 on this to get the content of the page.
        """
        response = self.session.get(url)
        return Response(response)


def connection(user_name, password):
    """
    This will method will authorize with topcoder and return a instance of Opener class.
    :param user_name:   Username of a topcoder account.
    :param password:    Password for the above user.
    :return:            Instance of a Opener class.
    """
    session = requests.Session()
    payload = """{
    \"username\":\"%s\",
    \"password\":\"%s\",
    \"client_id\":\"6ZwZEUo2ZK4c50aLPpgupeg5v2Ffxp9P\",
    \"scope\":\"openid profile offline_access\",
    \"response_type\":\"token\",
    \"connection\":\"TC-User-Database\",
    \"grant_type\":\"password\",
    \"device\":\"Browser\"
    }
    """ % (user_name, password)
    headers = {
        'origin': "https://accounts.topcoder.com",
        'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",  # NOQA
        'content-type': "application/json;charset=UTF-8",
        'accept': "*/*",
        'referer': "https://accounts.topcoder.com/member?retUrl=https:%2F%2Fwww.topcoder.com%2Fmy-dashboard%2F",
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "en-US,en;q=0.8,sv;q=0.6,fr;q=0.4,de;q=0.2",
        'cache-control': "no-cache"
        }
    r = session.request("POST", "https://topcoder.auth0.com/oauth/ro", data=payload, headers=headers)
    r_json = json.loads(r.text)
    id_token = r_json['id_token']
    refresh_token = r_json['refresh_token']
    payload2 = """
    {\"param\":
        {\"externalToken\":\"%s\",
        \"refreshToken\":\"%s\"}
    }
    """ % (id_token, refresh_token)
    headers2 = {
        'origin': "https://accounts.topcoder.com",
        'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",  # NOQA
        'content-type': "application/json;charset=UTF-8",
        'accept': "*/*",
        'referer': "https://accounts.topcoder.com/member?retUrl=https:%2F%2Fwww.topcoder.com%2Fmy-dashboard%2F",
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "en-US,en;q=0.8,sv;q=0.6,fr;q=0.4,de;q=0.2",
        'cache-control': "no-cache"
        }
    session.request("POST", "https://api.topcoder.com/v3/authorizations", data=payload2, headers=headers2)
    return Opener(session)
