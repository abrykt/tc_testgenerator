from .test_builder import TestBuilder
from .tc_types import Types


class CppTestBuilder(TestBuilder):

    def __init__(self):
        super(CppTestBuilder, self).__init__()
        self.data = ""
        self.test_case_name = ""

    def get_header(self):
        return ("#pragma once\n"
                "#include <assert.h>\n"
                "#include <functional>\n")

    @staticmethod
    def get_macro(definition):
        s = '#define TEST_{}()'.format(definition.problem_name.upper())
        s += "\\" + "\n"
        s += '{classname} __a;'.format(classname=definition.class_name)
        s += "\\" + "\n"
        s += 'using namespace std::placeholders;'
        s += "\\" + "\n"
        s += 'auto f = std::bind(&{class_name}::{func_name}, __a, {placeholders});'.format(
            class_name=definition.class_name,
            func_name=definition.method_name,
            placeholders=CppTestBuilder.get_placeholder(definition.parameters))
        s += "\\" + "\n"
        s += '{test_case_name}(f)\n\n'.format(
            test_case_name=CppTestBuilder.get_test_case_name(definition.problem_name))
        return s

    @staticmethod
    def get_placeholder(parameters):
        placeholders = ['_{}'.format(n + 1) for n, _ in enumerate(parameters)]
        return ', '.join(placeholders)

    @staticmethod
    def get_test_case_name(problem_name):
        return "{problem_name}Test".format(problem_name=problem_name)

    def get_tests(self, data, definition):
        s = self.get_macro(definition)
        s += "void {test_case_name}(std::function<{function_signature}> f)\n".format(
            test_case_name=self.get_test_case_name(definition.problem_name),
            function_signature=self.to_std_function_template_parameter(definition))
        s += "{\n"

        for test_data in data:
            s += self.get_test_body(
                parameters=CppTestBuilder.to_parameter_string(test_data['input'], definition.parameters),
                expected=CppTestBuilder.formatter(test_data['output'], definition.return_type)
            )
            s += '\n'
        s += '}'

        return s

    @staticmethod
    def get_test_body(parameters, expected):
        s = "\tassert(f({parameters}) == {expected});".format(
            parameters=parameters,
            expected=expected)
        return s

    @staticmethod
    def to_parameter_string(parameters, types):
            s = [CppTestBuilder.formatter(p, types[index]) for index, p in enumerate(parameters)]
            return ', '.join(s)

    @staticmethod
    def formatter(value, t):
        """
        :param value: Value to be formatted.
        :param t: The type of the value.
        :return: A string which can be passed as input to a parameter in a C++ function
        """
        if t == Types.String:
            s = '\"{}\"'.format(value)
            return s
        if isinstance(t, Types.Array):
            return "{type}({value})".format(type=CppTestBuilder.to_cpp_type(t), value=value)
        else:
            return value

    @staticmethod
    def to_cpp_type(t):
        if isinstance(t, Types.Array):
            return "std::vector<{template_parameter}>".format(
                template_parameter=CppTestBuilder.to_cpp_type(t.type))
        elif t == Types.String:
            return "std::string"
        elif t == Types.Int:
            return "int"
        elif t == Types.Long:
            return "long"
        elif t == Types.Float:
            return "float"
        elif t == Types.Double:
            return "double"
        else:
            raise Exception("No type available.")

    @staticmethod
    def to_std_function_template_parameter(definition):
        return "{return_type}({input_parameters})".format(
            return_type=CppTestBuilder.to_return_type(definition),
            input_parameters=CppTestBuilder.to_input_parameters(definition)
        )

    @staticmethod
    def to_return_type(definition):
        return CppTestBuilder.to_cpp_type(definition.return_type)

    @staticmethod
    def to_input_parameters(definition):
        s = [CppTestBuilder.to_cpp_type(t) for t in definition.parameters]
        return ', '.join(s)