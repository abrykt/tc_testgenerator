import re
from .tc_types import Types
import json


class Definition(dict):
    def __init__(self, *args, **kwargs):
        super(Definition, self).__init__(*args, **kwargs)
        self.__dict__ = self

        self.allowed_keywords = [
            'class_name',
            'method_name',
            'parameters',
            'return_type',
            'problem_name',
        ]
        self._check_keywords(kwargs)

    def _check_keywords(self, keyword_args):
        for keyword in keyword_args:
            if keyword not in self.allowed_keywords:
                raise Exception('Keyword {keyword} not allowed.'.format(keyword=keyword))

    @classmethod
    def create_from_file(cls, path_to_file):
        with open(path_to_file, 'r') as f:
            content = f.read()
            json_dict = json.loads(content)
            definition_dict = json_dict['definition']
            class_name = definition_dict['class_name']
            method_name = definition_dict['method_name']
            parameters = definition_dict['parameters']
            return_type = definition_dict['return_type']
            problem_name = definition_dict['problem_name']
            return cls(class_name=class_name,
                       method_name=method_name,
                       parameters=parameters,
                       return_type=return_type,
                       problem_name=problem_name)


class Parser(object):

    def __init__(self):
        self.class_reg = r'Class:\|(.*)\n'
        self.returns_reg = r'Returns:\|(.*)\n'
        self.method_reg = r'Method:\|(.*)\n'
        self.params_reg = r'Parameters:\|(.*)\n'

    def parse(self, problem_name, txt):
        definition = Definition(class_name=self.parse_class_name(txt),
                                method_name=self.parse_method_name(txt),
                                parameters=self.parse_parameter(txt),
                                return_type=self.parse_return_type(txt),
                                problem_name=problem_name)
        return definition

    def parse_method_name(self, txt):
        return self.reg_parse(txt, self.method_reg)

    def parse_parameter(self, txt):
        s = self.reg_parse(txt, self.params_reg)
        return self.parse_type(s)

    def parse_class_name(self, txt):
        return self.reg_parse(txt, self.class_reg)

    def parse_return_type(self, txt):
        s = self.reg_parse(txt, self.returns_reg)
        return self.parse_type(s)[0]

    @staticmethod
    def reg_parse(txt, regex):
        p = re.compile(regex)
        m = p.findall(txt)
        s = m[0].lstrip().rstrip()
        return str(s)

    @staticmethod
    def parse_type(txt):
        """
        Takes a string of types and returns a list of types
        :param txt:     String of types. For example:
                        "String, String[], Int"
        :return:        A list of types. for the above example this method would return:
                        [Types.String, Types.Array(Types.String), Types.Int]
        """
        list_of_types = txt.split(',')
        return [Parser.to_type(t.lstrip().rstrip()) for t in list_of_types]

    @staticmethod
    def is_array(txt):
        return txt.endswith('[]')

    @staticmethod
    def to_type(txt):
        if Parser.is_array(txt):
            return Types.Array(Parser.to_type(txt[:-len('[]')]))
        elif txt == 'String':
            return Types.String
        elif txt == 'int':
            return Types.Int
        elif txt == 'long':
            return Types.Long
        elif txt == 'float':
            return Types.Float
        elif txt == 'double':
            return Types.Double
