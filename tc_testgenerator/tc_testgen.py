import os
import sys
import getpass
import json
from .tc_parser import Parser
from .tc_parser import Definition
from .cpp_test_builder import CppTestBuilder
from .connection import connection
from .utils import make_dir
from .tc_scraper import TcScraper


class TcTestGen(object):

    @staticmethod
    def save_data(path, definition, test_data):
        directory_name = os.path.dirname(path)
        make_dir(directory_name)
        to_write = dict()
        to_write['test_data'] = test_data
        to_write['definition'] = dict(definition)
        with open(path, 'w') as f:
            f.write(json.dumps(to_write))

    @staticmethod
    def create_tests(problem_name, test_data, definition):
        builder = CppTestBuilder()
        output_folder_name = 'output'
        make_dir(output_folder_name)
        filename = "{}.{}".format(problem_name.lower(), 'hpp')
        path = os.path.join(output_folder_name, filename)
        builder.write_tests(path, test_data, definition)

    @staticmethod
    def create_from_name(problem_name, user_name, password, path_for_saving=False):
        opener = connection(user_name, password)

        scraper = TcScraper(opener=opener)
        pages = scraper.get_pages(search_query=problem_name)

        problem_statement = TcScraper.get_problem_statement(pages['problem_statement'])

        definition = Parser().parse(problem_name=problem_name, txt=problem_statement)
        test_data = TcScraper.get_system_test_data(pages['top_submission'], definition=definition)

        if path_for_saving:
            TcTestGen.save_data(definition=definition, test_data=test_data, path=path_for_saving)

        # save_files(problem_name, problem_statement=problem_statement['problem_statement'], data=test_data)
        TcTestGen.create_tests(problem_name=problem_name, definition=definition, test_data=test_data)

    @staticmethod
    def create_from_file(path_to_file):
        definition = Definition.create_from_file(path_to_file=path_to_file)
        test_data = TcTestGen.read_test_data(path_to_file=path_to_file)
        TcTestGen.create_tests(problem_name=definition.problem_name, definition=definition, test_data=test_data)

    @staticmethod
    def read_test_data(path_to_file):
        with open(path_to_file, 'r') as f:
            content = f.read()
            dict_content = json.loads(content)
            test_data = dict_content['test_data']
            return test_data

    @staticmethod
    def check_keys(keys, dictionary):
        for key in keys:
            if key in dictionary.keys() and dictionary[key] is not None:
                pass
            else:
                return False
        return True

    @staticmethod
    def get_input_parameters(parser):

        (options, args) = parser.parse_args()
        values = vars(options)

        if TcTestGen.check_keys(['file'], values):
            return None, None, None, options.file, None

        elif TcTestGen.check_keys(['username', 'problem_name'], values):

            if not options.password:
                password = getpass.getpass("password:")
            else:
                password = options.password

            if options.path_for_saving:
                path_for_saving = options.path_for_saving
            else:
                path_for_saving = None

            return options.problem_name, options.username, password, None, path_for_saving

        else:
            parser.print_help()
            sys.exit()


