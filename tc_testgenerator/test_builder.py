class TestBuilder(object):

    def __init__(self):
        self.message = """// This is a generated file.\n"""

    def get_header(self):
        return ""

    def get_tests(self, data, definition):
        raise NotImplemented()

    def get_method_format_string(self, definition):
        raise NotImplemented()

    def write_tests(self, path, data, definition):
        with open(path, 'w') as f:
            f.write(self.message)
            f.write(self.get_header())
            f.write('\n')
            f.write(self.get_tests(data, definition))
            f.write('\n\r')
        print("Tests generated in {path}".format(path=path))
