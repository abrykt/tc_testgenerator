class Types(object):
    def __init__(self):
        pass

    String = 'str'
    Int = 'int'
    Float = 'float'
    Long = 'long'
    Double = 'double'

    class Array(list):
        def __init__(self, *args):
            self.type = args[0]
            list.__init__(self, [self.type])