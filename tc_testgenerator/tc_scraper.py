"""
This file is based of the work of Kosei Moriyama (https://gist.github.com/cou929/514061)
"""

from bs4 import BeautifulSoup
import html2text
import re
from .tc_types import Types

prefix_search_from = 'http://www.topcoder.com/tc?module=ProblemArchive&class='
prefix_problem_statement = 'http://www.topcoder.com/stat?c=problem_statement&pm='
url_round_result_page = 'http://www.topcoder.com/tc?module=ProblemDetail&rd=<rd>&pm=<pm>'
url_problem_solution = 'http://www.topcoder.com/stat?c=problem_solution&cr=<cr>&rd=<rd>&pm=<pm>'


class TcScraper(object):

    def __init__(self, opener):
        """
        Class to parse system test data given a topcoder problem name.
        :param opener:  An instance of a class that provides method open() that, given a topcoder.com url, returns a
                        class instance that provides method read() to read the content of the page.
                        The instance should provide a session without need to input username / password.
        """
        self.opener = opener

    def search_problem(self, search_query):
        """
        Search topcoder to a problem
        :param search_query:    Problem name.
        :return:                A page containing all problems that matched the problem name.
        """
        res = self.opener.open(prefix_search_from + search_query)
        return res.read()

    @staticmethod
    def get_problem_and_round_id(html):
        """
        Get the id of the problem and the id of the competition round where the problem was presented. If the html page
        contains multiple matches then ids for the first match are returned.
        :param html: Html text containing search page for the class name of the problem.
        :return: Dictionary containing keys 'round_id' and 'problem_id'.
        """
        r = re.findall('"/tc\?module=ProblemDetail&rd=([0-9]+?)&pm=([0-9]+?)"\sclass="statText"', html, re.I)
        r = r[0]  # Pick first if more than one.
        m = {'round_id': r[0], 'problem_id': r[1]}
        return m

    def get_problem_parameters(self, search_query):
        return self.get_problem_and_round_id(self.search_problem(search_query))

    def get_problem_statement_page(self, problem_id):
        """
        Get html page containing the problem statement page.
        :param problem_id:  Id of the problem.
        :return:            Html page with the problem statement.
        """
        return self.opener.open(prefix_problem_statement + problem_id).read()

    def get_round_result_rage(self, problem_id, round_id):
        """
        Lets you retreive the page showing results for a competition round and problem.
        :param problem_id:  The problem id
        :param round_id:    The competition round id containing the problem.
        :return:            The html page.
        """
        url = url_round_result_page.replace('<pm>', problem_id).replace('<rd>', round_id)
        res = self.opener.open(url)
        return res.read()

    @staticmethod
    def get_top_submission_id(html):
        r = 'href="/stat\?c=problem_solution&(?:amp;)*cr=([0-9]+?)&(?:amp;)*rd=[0-9]+?&(?:amp;)*pm=[0-9]+?" class="statText"'  # NOQA
        return re.findall(r, html, re.I)

    def get_submission_page(self, problem_id, round_id, submission_id):
        """
        Lets you retrieve the html page showing the results given a problem, competition round id and a submission id.
        :param problem_id:          The problem id.
        :param round_id:            The competition round where the problem was presented.
        :param submission_id:   The submission id to retreive data for.
        :return:                    Html page with information about the submission for the given problem / round.
        """
        url = url_problem_solution.replace('<pm>', problem_id).replace('<rd>', round_id).replace('<cr>', submission_id)
        res = self.opener.open(url)
        return res.read()

    def get_pages(self, search_query):
        """
        Find html documents containing information about a topcoder problem and the system test data.
        :param search_query:    The problem name.
        :return:                Dictionary with keys 'problem_statement' and 'top_submission'
                                'problem_statement' contains the page with a description of the problem.
                                'top_submission' contains the page with system test data.
        """
        pages = {}
        params = self.get_problem_parameters(search_query)
        if not params:
            raise Exception()
        top_html = self.get_round_result_rage(params['problem_id'], params['round_id'])
        top_ids = self.get_top_submission_id(top_html)
        pages['problem_statement'] = self.get_problem_statement_page(params['problem_id'])
        pages['top_submission'] = self.get_submission_page(params['problem_id'], params['round_id'], top_ids[0])
        return pages

    @staticmethod
    def get_problem_statement(html):
        """
        Find the problem statement in a htlm document from topcoder website.
        :param html: The html document.
        :return: String the problem statement.
        """
        soup = BeautifulSoup(html, 'html.parser')
        problem_statement = soup.find('td', {'class': 'problemText'}).table
        problem_statement = str(problem_statement)
        return html2text.html2text(problem_statement)

    @staticmethod
    def type_to_regex(t):
        """
        Converts a type to a regex string that can be used to match the type.
        :param t:   The type.
        :return:    Regex match group.
        """
        if t in [Types.Int, Types.Long, Types.Float, Types.Double]:
            return r"(.*)"
        elif t == Types.String:
            return r'"(.*)"'
        elif isinstance(t, Types.Array):
            return r"({.*})"
        else:
            raise Exception("Unhandled type: {type}".format(type=type(t)))

    @staticmethod
    def types_to_regex(types):
        """"
        Converts a list of types to a regex string that can be used to parse a string for values matching the types.
        :param types:   List of types.
        :return:        Regex string.
        """
        delimiter_reg = "(,)"
        reg = r""
        if isinstance(types, list):
            for t in types:
                reg += TcScraper.type_to_regex(t)
                reg += delimiter_reg
            reg = reg[:-len(delimiter_reg)]
        else:
            reg += TcScraper.type_to_regex(types)
        return reg

    @staticmethod
    def format_string(s):
        s = s.lstrip().rstrip()
        s = s.replace('\n', '')
        return s

    @staticmethod
    def reg_parse(txt, regex):
        """
        :param txt:     Text to parse from.
        :param regex:   Regex containing match groups.
        :return:        Returns a list of string that is matched in each group.
        """
        txt = TcScraper.format_string(txt)
        p = re.compile(regex)
        m = p.findall(txt)[0]
        m = list(m) if isinstance(m, tuple) else [m]
        return [value for value in m if value != ',']

    @staticmethod
    def parse(txt, types):
        """
        :param txt:     Text to parse.
        :param types:   List of types that should be parsed.
        :return:        Returns a list of parsed values if more than one value, otherwise the value is returned.
        """

        # Todo
        # Maybe check the type of the value and store list as list, ints as Int etc.
        reg = TcScraper.types_to_regex(types=types)
        values = TcScraper.reg_parse(txt, reg)

        if len(values) > 1:
            return values
        elif len(values) == 1:
            return values[0]
        else:
            raise Exception("Could not parse value.")

    @staticmethod
    def get_system_test_data(html, definition):
        """
        :param html:        Text html document containing the data.
        :param definition:  Instance of class Definition which defines the data.
        :return:            A list of dictionaries with the input and the  expected output for each testcase, each
                            dictionary contains keys 'input' and 'output with corresponding values.
        """
        soup = BeautifulSoup(html, 'html.parser')
        test_data = []

        lines = soup.find_all('tr', {'valign': 'top'})

        for line in lines:
            i = TcScraper.parse(line.contents[3].contents[0], definition.parameters)
            o = TcScraper.parse(line.contents[7].contents[0], definition.return_type)

            test = {
                'input': i,
                'output': o
            }
            test_data.append(test)

        return test_data
