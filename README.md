# tc_testgenerator #

A test generator written in python for C++ solutions to topcoder problems.


### Guide ###

*   Clone this repository (or download the content as a zip and extract).

    ```
    $ git clone https://bitbucket.org/abrykt/tc_testgenerator.git
    ```

*   Change directory to the root of this repository.

    ```
    $ cd tc_testgenerator
    ```

*   Install requirements

    You need python 3. In addition to that you need the modules defined in requirements.txt. You can install them with:

    ```
    $ pip install -r requirements.txt
    ```

*   Generate the test

    You need the name of the topcoder problem that you want to generate tests for. For
    [this](https://community.topcoder.com/stat?c=problem_statement&pm=13918) problem for example, the name is "Abba".

    You also need your password and username for your topcoder.com account.

    Generate with:

    ```
    $ python main.py -n <NAME OF TOPCODER PROBLEM> -u <USERNAME>
    ```

    This will ask you for your password and then generate a C++ header named <problem_name>.hpp in a folder named
    'output' in the root of the repository.

    There is also a possibility to save the test data to a file. You can also generate tests reading test data from a
    file, instead of reaching out to topcoder.com for the test data. This could be used for creating additional tests
    for an already existing problem or to add tests for a problem that does not exist on topcoder.com. Please see help
    for additional instructions.

    ```
    python main.py -h
    ```

*   Test your problem solution

    Add the folder 'output' to your C++ header includes. Then include the header 'abba.hpp' in your .cpp file.

        #include "abba.hpp"

    In your main function, use macro which has been generated for you. The macro is named like TEST_<PROBLEM_NAME>. In
    this example the name of the macro is TEST_ABBA. Call this macro to run the tests. For example:

        int main(int argc, char **argv)
        {
            TEST_ABBA();
        }

    Make sure to build your application in Debug mode as the tests are implemented using asserts.

    A full example including a solution to the problem 'Abba' could look like this:

        #include <string>
        #include <algorithm>
        #include "abba.hpp"

        class ABBA
        {
          public:
             std::string canObtain(std::string initial, std::string target)
             {
                 if(initial == target)
                 {
                     return "Possible";
                 }
                 else if (target.length() == 0)
                 {
                     return "Impossible";
                 }
                 else
                 {
                     std::string last_char = target.substr(target.size() - 1, target.size());
                     std::string substring = target.substr(0, target.size() -1);
                     if(last_char == "A")
                     {
                         return canObtain(initial, substring);
                     }
                     else // last char is "B"
                     {
                         std::reverse(substring.begin(), substring.end());
                         return canObtain(initial, substring);
                     }
                 }
             }
        };

        int main(int argc, char **argv)
        {
            TEST_ABBA();
        }

### Contribution guidelines ###

* If you want to add possibility to generate tests for other languages feel free to create a fork, or contact me
  (abrykt at gmail.com) if you want
  to add a branch and then merge to master.

