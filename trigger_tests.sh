#!/usr/bin/env bash
curl -X POST -is -u $USERNAME:$TRIGGER_PIPELINES_PASSWORD \
  -H 'Content-Type: application/json' \
 https://api.bitbucket.org/2.0/repositories/abrykt/tc_testgenerator_test/pipelines/ \
  -d '
  {
    "target": {
      "ref_type": "branch",
      "type": "pipeline_ref_target",
      "ref_name": "master"
    }
  }'